import unittest
from city_function import get_formatted_name

class NameTestCase(unittest.TestCase):
	def test_city_country(self):
		formatted_name=get_formatted_name('santiago','chile')
		self.assertEqual(formatted_name,'Santiago,Chile')
	def test_city_country_population(self):
		formatted_name=get_formatted_name('santiago','chile',50000)
		self.assertEqual(formatted_name,'Santiago,Chile-population 50000')
		
unittest.main()
