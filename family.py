import cPickle as p

class family():
    def __init__(self,name,age,sex,email,telephone):
        self.name=name
        self.age=age
        self.sex=sex
        self.email=email
        self.telephone=telephone
    def mod_name(self,name):
        self.name=name
    def mod_age(self,age):
        self.age=age
    def mod_sex(self,sex):
        self.sex=sex
    def mod_email(self,email):
        self.email=email
    def mod_telephone(self,telephone):
        self.telephone=telephone
    def print_info(self):
        print 'Here is my information:'
        print 'My name is %s.' % self.name
        print 'I am %d years old.' % self.age
        print 'I am a %s.' % self.sex
        print 'My email address is %s.' % self.email
        print 'My telephone number is %d.' % self.telephone

my_family={}
my_family['meng']=family('meng',30,'male','jiaoxm01@126.com',15110262881)
my_family['meng'].print_info()
f=open('family.data','w')
p.dump(my_family,f)
f.close()
